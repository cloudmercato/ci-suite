#!/usr/bin/env python
import io


class FileGenerator(io.IOBase):
    mode = 'rb'

    def __init__(self, size=1, name=None, buffering=1):
        self.size = size
        self.offset = 0
        self.file = open(name, 'rb', buffering)
        self.buffering = buffering

    def read(self, size=None):
        if self.offset >= self.size:
            return b''
        left = self.size - self.offset
        if size is None:
            self.offset = self.size
            return self.file.read(left)
        else:
            self.offset += size
            if self.offset > self.size:
                self.offset = self.size
            return self.file.read(size)[:left]

    def readable(self):
        return True

    def seekable(self):
        return True

    def seek(self, offset, whence=0):
        if whence == 0:
            self.offset = offset
        elif whence == 1:
            self.offset += offset
        elif whence == 2:
            self.offset = self.size - self.offset

    def close(self):
        self.file.close()

    @property
    def closed(self):
        return self.file.closed

    def tell(self):
        return self.offset


def test(iterations, size, chunk_size, filename):
    for i in range(iterations):
        gen = FileGenerator(size=size, name=filename)
        while True:
            gen.read(chunk_size)
            if gen.offset == size:
                break

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--size', default=2**20, type=int)
    parser.add_argument('-c', '--chunk-size', default=2**16, type=int)
    parser.add_argument('-i', '--iterations', default=1, type=int)
    parser.add_argument('-n', '--filename', default='/dev/urandom')
    args = parser.parse_args()
    test(iterations=args.iterations, size=args.size, chunk_size=args.chunk_size,
         filename=args.filename)



if __name__ == '__main__':
    import timeit
    print(timeit.timeit("main()", setup="from __main__ import main",
                        number=1))
